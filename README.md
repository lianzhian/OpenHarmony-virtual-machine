# OpenHarmony虚拟机环境

#### 介绍

OpenHarmony虚拟机环境。包括3.0 和最新master分支的虚拟机。

如果下载链接过期，请联系 13510979604


### （2）适用于OpenHarmony 3.0 以上版本

此版本适用于 3.0 以上版本，下载链接：

====Hi3861 Docker镜像====
https://gitee.com/hihope_iot/docs/blob/master/HiSpark_WiFi_IoT/Software/Hi3861-docker.md


（2）VirtualBox的

链接：https://pan.baidu.com/s/1DmtVdJ7ZLprOFvnpReVbyw 

提取码：y8xj

账号：hihope

密码：123456

代码路径：~/openharmony


已经配置好环境，下载好代码，编译可通过。

已经下载好了repo，开发者只需要自己配置git，即可下载代码


### （3）适用于最新的master版本

第2部分提供的虚拟机环境可以适用于最新的master 分支，只需要下载master分支的代码，然后

执行 pip uninstall ohos-build

接着在master分支目录下，执行 pip install build/lite

看到如下图示表示成功：


```
Successfully built ohos-build
Installing collected packages: ohos-build
Successfully installed ohos-build-0.4.6

```



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
